# SOCIAL DATA SCRAPPER

## INTRODUCTION AND UNEDERSTANDING THE PROJECT
### Main goal of the project   
<br>
The goal of this project is provide a tool that allows user extract data from multiple social media sites and save them in DB. Then data can be analyzed through a dashboard graphical interface. 
Data will exposed to dashboard using a REST API that will get data from DB. 
In the dasboard user also can select which data and from where he want extract the data.   
<br>
<br>

### Tech stack
The project is entirely orchestred by Docker using docker-compose. The file "docker-compose.yml" will be the entrypoint to run the application and where all parts of the project are specified. 
The folder gonna contain, at least db, api/scrapper and dashboard setup instructions.

The technological stack used for this project is: 
- Docker for build and run all apps in project.
- HTML/JS/CSS powered with Electron and React for the dashboard.
- Python with FastAPI/Websockets/SqlAlchemy for the backend and rest API.
- Third party libraries for scrapping when available.
- Oracle to persist data.

### Folder structure
We can find 3 folders at the project root.   
Less important folder here is "migrations" that contains some instructions to database setup. Creation of users and schemas mainly.   

Next folder is "dashboard". This folder contains the frontend app and have inside their dependencies and source code. When this part of the project be done more explanations about it's structure will be provided.   

The last and more importante folder is "app". Contains the RestAPI and the code used for scrapping social media sites.

Inside app folder we find the file "main.py" that is used as entrypoint of the application and initilizes and import anything required. We also can see a requirements.txt file containing Python dependiencies. This file is critical to setup dev environment and to create the Docker image of the application.  
Dockerfile is a simple bunch of instructions to compile an image. Anything special there except the download and place of the Instantclient to use Oracle database in the application.  
Last file at this level is "config.py" that will gather and validate any value extracted from environment variables (from .env or directly from exported vars). The content will be saved in a variable that will be shared across the application.  
<br>
Special atention now for each folder in this directory:
- "db": contains the files needed to initialize database (create tables) and load initial static data. These jobs are made by files starting by underscore. Rest of the files are used for get session to connect to database at other parts of the application.
- "models": inside this folder are placed a file for each entity represented in the database. The library used to translate the tables (entities) to a Python like objects is sqlalchemy.    
- "schemas": similar structure of models but used for handle requests and formatting responses to client. They also helps to mantain cohesion in the differents parts of the application using everytime the same classes to type variables.   
- "helpers": miscelaneous folder where most important files are scrapper files (one for each social media included in the project and another as base for common functionalities).   
- "crud": one file for each database entity with the basic operations (Read, Create, Update, Delete).   
- "api": where endpoints are defined with FastApi Router and requests are handled in each endpoint file.   


<br>

## PROJECT SETUP
<br>
To run the project first some parts need to be build, like backend image.    

To start build stage we just need to open a terminal at the root of the project and run: 
```bash
docker-compose build
```
When backend image is built. We need to run all the services specified in the docker-compose.yml file.   
To do that just type in the same terminal:
```bash
docker-compose up
```
IMPORTANT: this process will take a long time first time is ran. Oracle image needs to be downloaded (7.5gb) and start local database. If some issue is raised in download process, just write a mail to dmorales474@gmail.com because image lives in his Docker registry.

Also multiple environment variables must be provided to application. Can be added to the current environment variables with EXPORT or SET. But is recommended to place a .env file in the root of the project that will be automatically loaded.    
An exanmple of a .env file with all needed vars is:

```text
SERVER_HOST="http://localhost"
PROJECT_NAME="social-data-scrapper"
DB_HOST="0.0.0.0"
DB_DIALECT="oracle"
DB_DRIVER="cx_oracle"
DB_USER="SOCIALDATA"
DB_PASSWORD="SOCIALDATA"
DB_SERVICE_NAME="orclpdb1"
DB_PORT="1521"
DB_NAME="SOCIALDATA"
DB_DRIVER_PATH="C:\\instantclient"
LD_LIBRARY_PATH=/opt/oracle
```
User, password, dbname, service name... are not configurable at this point. So these example values are the final values that will be provided to application.    
DB_DRIVER_PATH is only needed to develop in Windows. 


Once everything is setup correctly and docker-compose up command does not return any error, we can view RestAPI interfacte at http://localhost:8000/docs

<br>

## TASKS

### Next steps

- [ ] Testing backend part
- [ ] Create frontend with Electron and React
- [ ] Include frontend in docker-compose structure
- [ ] Testing frontend part
- [ ] Add more social apps. Instagram, Youtube, Facebook...
- [ ] Enable scrapping from topic.
- [ ] Adding security and user system to the API
- [ ] Enable endpoint to refresh data cached at application start (targets, sources...)
- [ ] Modify relations between entities to make cascade operations on update or delete operations
- [ ] Improve of "upsert" mechanism when loading scrapped data into DB. Using MERGE or similar from sqlalchemy.
- [ ] Add and condigure linter
- [ ] Remove "InDB" schemas if not used finally
- [ ] Add queue manager to handle long time jobs like scrapping. Will probably replace current websocket logic.



### Bug fixes

- [ ] Timestamp fields not working properly. Expects Timestamp type but recives number.
