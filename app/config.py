from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, validator


class Settings(BaseSettings):
    API_STR: str = "/api"

    SERVER_HOST: AnyHttpUrl

    BACKEND_CORS_ORIGINS: List[str] = ["*"]


    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)) or v == "*":
            return v
        raise ValueError(v)

    PROJECT_NAME: str

    DB_DIALECT: str
    DB_DRIVER: str
    DB_USER: str
    DB_PASSWORD: str
    DB_HOST: str
    DB_PORT: str
    DB_SERVICE_NAME: str
    SQLALCHEMY_DATABASE_URI: Optional[str] = None
    DB_DRIVER_PATH: Optional[str] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        return '{}+{}://{}:{}@{}:{}/?service_name={}'.format(
            values['DB_DIALECT'].lower(),
            values['DB_DRIVER'].lower(),
            values['DB_USER'],
            values['DB_PASSWORD'],
            values['DB_HOST'],
            values['DB_PORT'],
            values['DB_SERVICE_NAME']
        )

    SOURCES: Optional[dict] = None
    TARGETS: Optional[dict] = None

    class Config:
        case_sensitive = True
        env_file = ".env"


settings = Settings()