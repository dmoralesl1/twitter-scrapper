from .base import CRUDBase
from app.models.source import Source
from app.schemas.source import SourceCreate, SourceUpdate

source = CRUDBase[Source, SourceCreate, SourceUpdate](Source)