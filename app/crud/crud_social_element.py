from .base import CRUDBase
from app.models.social_element import SocialElement
from app.schemas.social_element import SocialElementCreate, SocialElementUpdate

social_element = CRUDBase[SocialElement, SocialElementCreate, SocialElementUpdate](SocialElement)