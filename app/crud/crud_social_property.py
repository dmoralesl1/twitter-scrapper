from .base import CRUDBase
from app.models.social_property import SocialProperty
from app.schemas.social_property import SocialPropertyCreate, SocialPropertyUpdate

social_element = CRUDBase[SocialProperty, SocialPropertyCreate, SocialPropertyUpdate](SocialProperty)