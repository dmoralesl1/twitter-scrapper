from .base import CRUDBase
from app.models.target import Target
from app.schemas.target import TargetCreate, TargetUpdate

target = CRUDBase[Target, TargetCreate, TargetUpdate](Target)