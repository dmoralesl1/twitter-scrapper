from .base import CRUDBase
from app.models.social_action import SocialAction
from app.schemas.social_action import SocialActionCreate, SocialActionUpdate

social_action = CRUDBase[SocialAction, SocialActionCreate, SocialActionUpdate](SocialAction)