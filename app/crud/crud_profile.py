from .base import CRUDBase
from app.models.profile import Profile
from app.schemas.profile import ProfileCreate, ProfileUpdate

profile = CRUDBase[Profile, ProfileCreate, ProfileUpdate](Profile)