from app.config import settings
from app import crud
from app.api.deps import get_db
from app.db.session import engine
from app.db._db_pre_start import init_db
from app.db._db_initial_data import fill_tables_initial_data

def init_app_context() -> None:
    session_db = next(get_db())

    # Running initial DB setup if tables are not created yet
    if not engine.has_table('sources'):
        init_db(engine)
        fill_tables_initial_data(session_db)


    # Loading sources and targets (not mutable data) into settings global object 
    # to being used in other helper functions.
    current_sources = crud.source.get_multi(session_db)
    current_targets = crud.target.get_multi(session_db)

    # Storing only needed data with minimal structure to find it easier.
    sources_cleaned = { source.name.upper(): source.id for source in current_sources}

    targets_cleaned = { target.name.upper(): target.id for target in current_targets}

    settings.SOURCES = sources_cleaned
    settings.TARGETS = targets_cleaned
