from typing import List
from datetime import datetime

import twint
import nest_asyncio
nest_asyncio.apply()

from app import schemas
from app.config import settings
from .base_scrapper import BaseScrapper

class TwitterScrapper(BaseScrapper):

    def __init__(self, limit: int=100):
        super().__init__('twitter')
        self.limit = limit

    # Get N tweets from a profile and return raw response
    def get_tweets_from_profile(self, username: str) -> List[twint.tweet.tweet]:
        self.__init_config()
        self.config.Username = username
        # self.config.Store_object_tweets_list = tweets
        twint.run.Search(self.config)
        return twint.output.tweets_list

    # Get N tweets from a topic (no matter which profile is) and return raw response
    def get_tweets_from_topic(self, topic: str) -> List[twint.tweet.tweet]:
        self.__init_config()

        self.config.Username = None
        self.config.Search = topic
        twint.run.Search(self.config)
        
        return twint.output.tweets_list


    def get_profile_info(self, username: str) -> twint.user.user:
        self.__init_config()

        self.config.User_full = True
        self.config.Username = username
        twint.run.Lookup(self.config)
        # self.__clean_config()
        return twint.output.users_list[0]
        

    # Get tweets from a profile and fill BaseScrapper fields to be used in insert db function later
    def fill_profile_info(self, username: str) -> None:
        profile = self.get_profile_info(username)
        tweets = self.get_tweets_from_profile(username)
        
        self.profiles.append(schemas.ProfileCreate(
            id=profile.id, 
            username=profile.username, 
            source_id=self.source_id,
            created_at=datetime.timestamp(
                datetime.strptime(f'{profile.join_date} {profile.join_time.split(" ")[0]}', '%Y-%m-%d %H:%M:%S')
                )))
        self.social_properties.append(schemas.SocialPropertyCreate(
            profile_id=profile.id,
            target_id=settings.TARGETS['FOLLOWERS'],
            value=str(profile.followers),

        ))
        self.social_properties.append(schemas.SocialPropertyCreate(
            profile_id=profile.id,
            target_id=settings.TARGETS['FOLLOWING'],
            value=str(profile.following),
        ))
        self.social_properties.append(schemas.SocialPropertyCreate(
            profile_id=profile.id,
            target_id=settings.TARGETS['TWEETS'],
            value=str(profile.tweets),
        ))      

        for tweet in tweets:
            # Tweet as social element
            self.social_elements.append(schemas.SocialElementCreate(
                id=tweet.id_str,
                profile_id=profile.id,
                target_id=settings.TARGETS['TWEET'],
                event_date=datetime.timestamp(datetime.strptime(f'{tweet.datestamp} {tweet.timestamp}', '%Y-%m-%d %H:%M:%S'))
            ))
            # Each part of tweet as social action (likes, retweets, images...)
            # Likes
            self.social_actions.append(schemas.SocialActionCreate(
                element_id=tweet.id_str,
                target_id=settings.TARGETS['LIKE'],
                value=str(tweet.likes_count)
            ))
            # Retweets
            self.social_actions.append(schemas.SocialActionCreate(
                element_id=tweet.id_str,
                target_id=settings.TARGETS['RETWEET'],
                value=str(tweet.retweets_count)
            ))
            # Tweet text content
            self.social_actions.append(schemas.SocialActionCreate(
                element_id=tweet.id_str,
                target_id=settings.TARGETS['CONTENT'],
                value=str(tweet.tweet)
            ))
            # URL of resource
            self.social_actions.append(schemas.SocialActionCreate(
                element_id=tweet.id_str,
                target_id=settings.TARGETS['RESOURCE_URL'],
                value=str(tweet.link)
            ))
            # Images
            for image_url in tweet.photos:
                self.social_actions.append(schemas.SocialActionCreate(
                    element_id=tweet.id_str,
                    target_id=settings.TARGETS['IMAGE'],
                    value=str(image_url)
                ))
            # URL in tweet
            for url in tweet.urls:
                self.social_actions.append(schemas.SocialActionCreate(
                    element_id=tweet.id_str,
                    target_id=settings.TARGETS['URL'],
                    value=str(url)
                ))
            # Hashtags
            for hashtag in tweet.hashtags:
                self.social_actions.append(schemas.SocialActionCreate(
                    element_id=tweet.id_str,
                    target_id=settings.TARGETS['HASHTAG'],
                    value=str(hashtag)
                ))
            # Mentions
            for mention in tweet.mentions:
                self.social_actions.append(schemas.SocialActionCreate(
                    element_id=tweet.id_str,
                    target_id=settings.TARGETS['MENTION'],
                    value=str(mention)
                ))

    # Get tweets from a topic and fill BaseScrapper fields to be used in insert db function later
    def fill_topic_info(self, topic: str) -> None:
        raise NotImplementedError

        
    def __init_config(self) -> twint.Config:
        self.config = twint.Config()
        self.config.Store_object = True
        self.config.Limit = self.limit
