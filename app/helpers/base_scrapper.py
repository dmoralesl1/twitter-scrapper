from app.config import settings
from app import crud
from app.api.deps import get_db
from app.helpers.custom_exceptions import NotPresentInDB

class BaseScrapper:
    def __init__(self, source: str):
        self.source = source
        self.source_id = settings.SOURCES[self.source.upper()]
        self.profiles = []
        self.social_actions = []
        self.social_elements = []
        self.social_properties = []

    def save_results(self):
        # @TODO: Remove try/except to control if primary key exists or constraint is violated
        db = next(get_db())

        for profile in self.profiles:
            try:
                crud.profile.create(db=db, obj_in=profile)
            except:
                db.rollback()
                db_profile = crud.profile.get(db=db, id=profile.id)
                if not db_profile:
                    raise NotPresentInDB(f'Profile with name {profile.username} not present in DB but cannot insert it')

                crud.profile.update(db=db, db_obj=db_profile, obj_in=profile)

        for social_element in self.social_elements:
            try:
                crud.social_element.create(db=db, obj_in=social_element)
            except:
                db.rollback()
                db_social_element = crud.social_element.get(db=db, id=social_element.id)
                if not db_social_element:
                    raise NotPresentInDB(f'Social element with id {db_social_element.id} not present in DB but cannot insert it')

                crud.social_element.update(db=db, db_obj=db_social_element, obj_in=db_social_element)
        
        for social_action in self.social_actions:
            try:
                crud.social_action.create(db=db, obj_in=social_action)
            except:
                db.rollback()
                db_social_action = crud.social_action.get(db=db, id=social_action.id)
                if not db_social_action:
                    raise NotPresentInDB(f'Social action with id {social_action.id} not present in DB but cannot insert it')

                crud.social_action.update(db=db, db_obj=db_social_action, obj_in=social_action)

        for social_property in self.social_properties:
            try:
                crud.social_property.create(db=db, obj_in=social_property)
            except:
                db.rollback()
                db_social_property = crud.social_property.get(db=db, id=social_property.id)
                if not db_social_property:
                    raise NotPresentInDB(f'Social property with id {social_property.id} not present in DB but cannot insert it')

                crud.social_property.update(db=db, db_obj=db_social_property, obj_in=social_property)


