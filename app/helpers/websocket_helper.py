import uuid
from typing import List

from fastapi import WebSocket

from app import schemas

class WebSocketWrapper(WebSocket):
    def __init__(self, websocket: WebSocket):
        self.web_socket = websocket
        self.user_id = uuid.uuid4()

    def __getattr__(self,attr):
        return getattr(self.web_socket, attr)

class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocketWrapper] = []

    async def connect(self, websocket: WebSocketWrapper):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocketWrapper):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, websocket: WebSocketWrapper, message: schemas.SocketMessage):
        await websocket.send_json(message.dict())

    async def broadcast(self, message: schemas.SocketMessage):
        for connection in self.active_connections:
            await connection.send_json(message.dict())