import enum
from typing import Optional

from pydantic import BaseModel

class TargetTypeEnum(enum.Enum):
    action = 'action'
    element = 'element'
    property = 'property'

# Shared properties
class TargetBase(BaseModel):
    name: Optional[str] = None
    type: Optional[TargetTypeEnum] = None

# Properties to receive on item creation
class TargetCreate(TargetBase):
    id: int
    name: str
    type: TargetTypeEnum


# Properties to receive on item update
class TargetUpdate(TargetBase):
    pass


# Properties shared by models stored in DB
class TargetInDBBase(TargetBase):
    id: int
    name: str
    type: TargetTypeEnum

    class Config:
        orm_mode = True


# Properties to return to client
class Target(TargetInDBBase):
    pass


# Properties properties stored in DB
class TargetInDB(TargetInDBBase):
    pass