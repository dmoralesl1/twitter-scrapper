import enum
from typing import List, Optional, Dict

from pydantic import BaseModel

class SocketError(BaseModel):
    has_error: Optional[bool] = False
    message: Optional[str] = None

class SocketMessage(BaseModel):
    '''
        Socket message is the object that is sent to websocket to subscribe to scrapping process
        Each client that wants subscribe to scrapping messages output can send a source to analyze 
        and the profiles/topics him is interested on. 
        Also can provide a bool param "viewer" that will make not trigger a scrapping process and 
        just connect to websocket to wait for messages generated from other processes. 
        Status attribute will be used to notifiy client that process is running or finished.
    '''
    source: str 
    profiles: Optional[List[str]] = None
    topics: Optional[List[str]] = None
    viewer: Optional[bool] = False
    status: Optional[enum.Enum('running', 'finished')] = None
    message: Optional[str] = None
    error: Optional[SocketError] = None