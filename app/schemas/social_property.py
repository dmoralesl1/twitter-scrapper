from typing import Optional

from pydantic import BaseModel


# Shared properties
class SocialPropertyBase(BaseModel):
    profile_id: Optional[int] = None
    target_id: Optional[int] = None
    value: Optional[str] = None

# Properties to receive on item creation
class SocialPropertyCreate(SocialPropertyBase):
    profile_id: int
    target_id: int
    value: str

# Properties to receive on item update
class SocialPropertyUpdate(SocialPropertyBase):
    pass


# Properties shared by models stored in DB
class SocialPropertyInDBBase(SocialPropertyBase):
    id: int
    profile_id: int
    target_id: int
    value: str
    class Config:
        orm_mode = True


# Properties to return to client
class SocialProperty(SocialPropertyInDBBase):
    pass


# Properties properties stored in DB
class SocialPropertyInDB(SocialPropertyInDBBase):
    pass