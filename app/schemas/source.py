from typing import Optional

from pydantic import BaseModel, HttpUrl


# Shared properties
class SourceBase(BaseModel):
    id: Optional[int] = None
    name: Optional[str] = None
    reference_url: Optional[HttpUrl] = None
    main_target_id: Optional[int] = None


# Properties to receive on item creation
class SourceCreate(SourceBase):
    id: int
    name: str
    reference_url: HttpUrl
    main_target_id: int


# Properties to receive on item update
class SourceUpdate(SourceBase):
    pass


# Properties shared by models stored in DB
class SourceInDBBase(SourceBase):
    id: int
    name: str
    reference_url: HttpUrl
    main_target_id: int

    class Config:
        orm_mode = True


# Properties to return to client
class Source(SourceInDBBase):
    pass


# Properties properties stored in DB
class SourceInDB(SourceInDBBase):
    pass