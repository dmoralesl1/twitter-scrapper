from typing import Any, Optional
from datetime import datetime

from pydantic import BaseModel



# Shared properties
class SocialElementBase(BaseModel):
    id: Optional[str] = None
    target_id: Optional[int] = None
    profile_id: Optional[int] = None
    event_date: Optional[Any] = None
    

# Properties to receive on item creation
class SocialElementCreate(SocialElementBase):
    id: str
    target_id: int
    profile_id: int
    event_date: Any


# Properties to receive on item update
class SocialElementUpdate(SocialElementBase):
    pass


# Properties shared by models stored in DB
class SocialElementInDBBase(SocialElementBase):
    id: int
    target_id: int
    profile_id: int
    event_date: Any

    class Config:
        orm_mode = True


# Properties to return to client
class SocialElement(SocialElementInDBBase):
    pass


# Properties properties stored in DB
class SocialElementInDB(SocialElementInDBBase):
    pass