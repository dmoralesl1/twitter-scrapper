from typing import Optional

from pydantic import BaseModel


# Shared properties
class SocialActionBase(BaseModel):
    element_id: Optional[int] = None
    target_id: Optional[int] = None
    value: Optional[str] = None

# Properties to receive on item creation
class SocialActionCreate(SocialActionBase):
    element_id: int
    target_id: int
    value: str

# Properties to receive on item update
class SocialActionUpdate(SocialActionBase):
    pass


# Properties shared by models stored in DB
class SocialActionInDBBase(SocialActionBase):
    id: int
    element_id: int
    target_id: int
    value: str
    class Config:
        orm_mode = True


# Properties to return to client
class SocialAction(SocialActionInDBBase):
    pass


# Properties properties stored in DB
class SocialActionInDB(SocialActionInDBBase):
    pass