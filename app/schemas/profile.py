from typing import Optional, Any
from datetime import datetime

from pydantic import BaseModel


# Shared properties
class ProfileBase(BaseModel):
    id: Optional[str] = None
    username: Optional[str] = None
    source_id: Optional[int] = None
    created_at: Optional[Any] = None # timestamp


# Properties to receive on item creation
class ProfileCreate(ProfileBase):
    id: str
    username: str
    source_id: int
    created_at: Any


# Properties to receive on item update
class ProfileUpdate(ProfileBase):
    pass


# Properties shared by models stored in DB
class ProfileInDBBase(ProfileBase):
    id: str
    username: str
    source_id: int
    created_at: Any

    class Config:
        orm_mode = True


# Properties to return to client
class Profile(ProfileInDBBase):
    pass


# Properties properties stored in DB
class ProfileInDB(ProfileInDBBase):
    pass