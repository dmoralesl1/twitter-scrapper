from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Target])
def read_targets(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100
) -> Any:
    """
    Retrieve targets.
    """

    targets = crud.target.get_multi(
        db=db, skip=skip, limit=limit
    )

    return targets


@router.get("/{id}", response_model=schemas.Target)
def read_source(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Get target by ID.
    """
    targets = crud.target.get(db=db, id=id)

    return targets


@router.delete("/{id}", response_model=schemas.Target)
def delete_target(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Delete a target.
    """
    targets = crud.target.get(db=db, id=id)
    
    return targets