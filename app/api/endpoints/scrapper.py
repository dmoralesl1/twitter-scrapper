from fastapi import APIRouter, WebSocket, WebSocketDisconnect

from app import schemas
from app.helpers.websocket_helper import *
from app.helpers.twitter_scrapper import TwitterScrapper

router = APIRouter()

manager = ConnectionManager()

@router.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    websocket_wrapped = WebSocketWrapper(websocket)
    await manager.connect(websocket_wrapped)

    try:
        while True:
            data = await websocket_wrapped.receive_json()
            socket_message = schemas.SocketMessage(**data)

            # Not performing scrapping if client is just subscribing to messages
            if socket_message.viewer:
                continue

            # Returning error if client did not provide users nor topics
            if not socket_message.profiles and not socket_message.topics:
                await manager.send_personal_message(websocket_wrapped, {
                    'has_error': True,
                    'message': 'You must provide at least one profile or topic'
                })
                continue

            # Creating scrapper provider based on source from request
            source = socket_message.source.lower()
            if source == "twitter":
                scrapper_provider = TwitterScrapper()
            elif source == "instagram":
                print('TODO')
            elif source == "youtube":
                print('TODO')
            elif source == "facebook":
                print('TODO')
            # If source is not supported, returning error
            else:
                await ({
                    'has_error': True,
                    'message': 'Source not supported'
                })
                continue
                
            # Creating scrapper process
            socket_message.message = 'Starting scrapping process'
            socket_message.status = 'running'
            await manager.send_personal_message(websocket_wrapped, socket_message)
            
            # Scrapping profiles if provided
            if socket_message.profiles:
                socket_message.message = 'Scrapping profiles'
                await manager.send_personal_message(websocket_wrapped, socket_message)
                for profile in socket_message.profiles:
                    socket_message.message = f'Scrapping profile {profile}'
                    await manager.send_personal_message(websocket_wrapped, socket_message)
                    scrapper_provider.fill_profile_info(profile)

            # Scrapping topics if provided
            # if socket_message.topics:
            #     socket_message.message = 'Scrapping topics'
            #     await manager.send_personal_message(websocket_wrapped, socket_message)
            #     for topic in socket_message.topics:
            #         socket_message.message = f'Scrapping topic {topic}'
            #         await manager.send_personal_message(websocket_wrapped, socket_message)
            #         scrapper_provider.fill_topic_info(topic)

            # Saving scrapping results to db
            socket_message.message = 'Saving scrapping results to db'
            await manager.send_personal_message(websocket_wrapped, socket_message)
            scrapper_provider.save_results()

            # Notifying client that process is finished
            socket_message.message = 'Scrapping process finished'
            socket_message.status = 'finished'
            await manager.broadcast(socket_message)


    except WebSocketDisconnect:
        manager.disconnect(websocket_wrapped)
        await manager.broadcast(schemas.SocketMessage(
            source='twitter', 
            error={'message': f'User {websocket_wrapped.user_id} disconnected'})
            )

    except Exception as e:
        await manager.broadcast(schemas.SocketMessage(
            source='undefined', 
            error={'message': f'Error: {e}', 'has_error': True})
            )

