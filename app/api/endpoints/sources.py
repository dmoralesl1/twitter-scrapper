from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Source])
def read_sources(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100
) -> Any:
    """
    Retrieve sources.
    """

    sources = crud.source.get_multi(
        db=db, skip=skip, limit=limit
    )

    return sources


@router.get("/{id}", response_model=schemas.Source)
def read_source(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Get source by ID.
    """
    sources = crud.source.get(db=db, id=id)

    return sources


@router.delete("/{id}", response_model=schemas.Source)
def delete_source(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Delete a source.
    """
    sources = crud.source.get(db=db, id=id)
    
    return sources