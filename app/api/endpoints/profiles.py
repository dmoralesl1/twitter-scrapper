from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Profile])
def read_profiles(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 100
) -> Any:
    """
    Retrieve profiles.
    """

    profiles = crud.profile.get_multi(
        db=db, skip=skip, limit=limit
    )

    return profiles


@router.get("/{id}", response_model=schemas.Profile)
def read_profile(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Get profile by ID.
    """
    profile = crud.profile.get(db=db, id=id)

    return profile


@router.delete("/{id}", response_model=schemas.Profile)
def delete_profile(
    *,
    db: Session = Depends(deps.get_db),
    id: int
) -> Any:
    """
    Delete a profile.
    """
    profile = crud.profile.get(db=db, id=id)
    
    return profile