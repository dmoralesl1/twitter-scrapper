import os
from typing import Generator

import cx_Oracle

from app.db.session import SessionLocal
from app.config import settings

# Init oracle client with explicit path to instantclient only on windows.
# On Linux LD_LIBRARY_PATH should be set to include the instantclient libs.
if os.name == 'nt':
    cx_Oracle.init_oracle_client(settings.DB_DRIVER_PATH)



def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    except Exception as e:
        print(e)
    finally:
        db.close()
 