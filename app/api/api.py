from fastapi import APIRouter

from app.api.endpoints import profiles, scrapper, sources, targets

api_router = APIRouter()
api_router.include_router(profiles.router, prefix="/profiles", tags=["profiles"])
api_router.include_router(scrapper.router, prefix="/scrapper", tags=["websocket"])
api_router.include_router(scrapper.router, prefix="/sources", tags=["sources"])
api_router.include_router(scrapper.router, prefix="/targets", tags=["targets"])
