from sqlalchemy import Column, Integer, String, Enum
from sqlalchemy.orm import relationship

from app.db.base_class import Base

class Target(Base):
    """
    Class Target
    """
    __tablename__ = 'targets'

    id = Column(Integer, primary_key=True)
    name = Column(String(100), nullable=False)
    type = Column(Enum('action', 'element', 'property'), nullable=False)

