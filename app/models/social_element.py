from sqlalchemy import Column, Integer, ForeignKey, String, UniqueConstraint
from sqlalchemy.dialects.oracle import TIMESTAMP

from app.db.base_class import Base

class SocialElement(Base):
    """
    Class SocialElement
    """
    __tablename__ = 'social_elements'

    id = Column(Integer, primary_key=True, autoincrement=True)
    target_id = Column(Integer, ForeignKey('targets.id'))
    profile_id = Column(String(150), ForeignKey('profiles.id'))
    event_date = Column(TIMESTAMP, nullable=True)
    # We cannot repeat the original_id for the same target but can be repeated for different targets
    UniqueConstraint(id, target_id)