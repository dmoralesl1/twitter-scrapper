from sqlalchemy import Column, Integer, ForeignKey, String, UniqueConstraint
from sqlalchemy.dialects.oracle import CLOB

from app.db.base_class import Base

class SocialAction(Base):
    """
    Class SocialAction
    """
    __tablename__ = 'social_actions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    element_id = Column(Integer, ForeignKey('social_elements.id'))
    target_id = Column(Integer, ForeignKey('targets.id'))
    value = Column(CLOB, nullable=False)
    # We cannot repeat the element_id for the same target but can be repeated for different targets
    UniqueConstraint(element_id, target_id)