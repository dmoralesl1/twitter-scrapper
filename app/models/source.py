from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base

class Source(Base):
    """
    Class Source
    """
    __tablename__ = 'sources'

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
    name = Column(String(100), nullable=False)
    reference_url = Column(String(500), nullable=False)
    main_target_id = Column(Integer, ForeignKey('targets.id')) 
