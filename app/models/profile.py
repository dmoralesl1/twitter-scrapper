from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.dialects.oracle import TIMESTAMP

from app.db.base_class import Base

class Profile(Base):
    """
    Class Profile
    """
    __tablename__ = 'profiles'

    id = Column(String(150), primary_key=True)
    username = Column(String(100), nullable=False)
    created_at = Column(TIMESTAMP, nullable=True)
    source_id = Column(Integer, ForeignKey('sources.id'))
    # We cannot repeat the username for the same source but can be repeated for different sources
    UniqueConstraint(username, source_id)