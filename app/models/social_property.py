from sqlalchemy import Column, Integer, ForeignKey, String, UniqueConstraint
from sqlalchemy.dialects.oracle import CLOB

from app.db.base_class import Base

class SocialProperty(Base):
    """
    Class SocialProperty
    """
    __tablename__ = 'social_properties'

    id = Column(Integer, primary_key=True, autoincrement=True)
    profile_id = Column(String(150), ForeignKey('profiles.id'))
    target_id = Column(Integer, ForeignKey('targets.id'))
    value = Column(CLOB, nullable=False)
    # We cannot repeat the element_id for the same target but can be repeated for different targets
    UniqueConstraint(profile_id, target_id)