import sqlalchemy
from app.db import base

def init_db(engine: sqlalchemy.engine):
    with engine.begin() as conn:
        base.Base.metadata.create_all(conn)

