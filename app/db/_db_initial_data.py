"""
    Data for initial setup script run before first time app is run.
    Include all data in tables that will not be able to create or update
    via API.
    For example, sources (included by developer) and targets.
"""
from sqlalchemy.orm import Session

from app import crud, schemas

def fill_tables_initial_data(db: Session) -> None:
    # Targets (order matter because of foreign key in sources table)
    targets = [
        [1, 'TWEET', 'element'],
        [2, 'LIKE', 'action'],
        [3, 'RETWEET', 'action'],
        [4, 'CONTENT', 'action'],
        [5, 'RESOURCE_URL', 'action'],
        [6, 'IMAGE', 'action'],
        [7, 'URL', 'action'],
        [8, 'HASHTAG', 'action'],
        [9, 'MENTION', 'action'],
        [10, 'FOLLOWERS', 'property'],
        [11, 'FOLLOWING', 'property'],
        [12, 'TWEETS', 'property'],
    ]

    for target in targets:
        target_schema = schemas.TargetCreate(
            id=target[0],
            name=target[1],
            type=target[2]
        )
        crud.target.create(db, obj_in=target_schema)


    # Sources
    sources = [
        [1, 'TWITTER', 'https://twitter.com/', 1],
        [2, 'INSTAGRAM', 'https://instagram.com/', 2],
        [3, 'YOUTUBE', 'https://youtube.com/', 2],
        [4, 'FACEBOOK', 'https://facebook.com/', 10],
        [5, 'TIKTOK', 'https://tiktok.com/', 10],
    ]

    for source in sources:
        source_schema = schemas.SourceCreate(
            id=source[0],
            name=source[1],
            reference_url=source[2],
            main_target_id=source[3]
        )
        crud.source.create(db, obj_in=source_schema)